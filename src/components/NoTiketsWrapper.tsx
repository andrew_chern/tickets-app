import React from 'react'
import {StyledErrorMessage, StyledNoTicketsWrapper, StyledReuploadTickets, StyledOopsWrapper} from "./styled/StyledNoTicketsWrapper"
import { faGrinBeamSweat } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {useAppDispatch} from "../app/hooks";
import {fetchTicketsAsync} from "../features/TicketsList/ticketsSlice";

const NoTiketsWrapper = () => {
    const dispatch = useAppDispatch()
    const handleUploadTickets = () => {
        dispatch(fetchTicketsAsync())
    }
    return (
        <StyledNoTicketsWrapper>
            <StyledOopsWrapper>
                <FontAwesomeIcon className='oops-icon' icon={faGrinBeamSweat} />
                <StyledErrorMessage>Упс...Что-то пошло не так.</StyledErrorMessage>
            </StyledOopsWrapper>
            <StyledReuploadTickets onClick={() => handleUploadTickets()}>
                Загрузить билеты
            </StyledReuploadTickets>
        </StyledNoTicketsWrapper>
    )
}

export default NoTiketsWrapper