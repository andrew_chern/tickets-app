import React from 'react';
import {
    StyledCardHeader,
    StyledCarrier, StyledInfoData, StyledInfoItem, StyledInfoTitle,
    StyledPrice,
    StyledTripInfoWrapper,
    TicketCardWrapper
} from "./styled/StyledTicketCard.styled";
import CarrierLogo from '../assets/icons/S7 Logo.svg';
import {ITicket} from "../features/TicketsList/TicketsList";
import moment from "moment";

interface IProps {
    ticket: ITicket
}

const TicketCard = (props: IProps) => {
    const timeConvert = (n: number) => {
        let num = n;
        let hours = (num / 60);
        let rhours = Math.floor(hours);
        let minutes = (hours - rhours) * 60;
        let rminutes = Math.round(minutes);
        return  rhours + "ч" + " " + rminutes + "м";
    }
    const wordTypoHandler = (data: number) => {
        switch (data) {
            case 0:
                return 'пересадок'
            case 1:
                return 'пересадка'
            default:
                return 'пересадки'
        }
    }
    return (
        <TicketCardWrapper>
            <StyledCardHeader>
                <StyledPrice>{props.ticket.price} Р</StyledPrice>
                <StyledCarrier src={CarrierLogo}></StyledCarrier>
            </StyledCardHeader>
            {props.ticket.segments.map((segment, index) => {
                return(
                    <StyledTripInfoWrapper key={index}>
                        <StyledInfoItem>
                            <StyledInfoTitle>{segment.origin} – {segment.destination}</StyledInfoTitle>
                            <StyledInfoData>
                                {moment(segment.date).format('HH:MM')} – {moment(new Date(new Date(segment.date).getTime() + segment.duration*60000)).format('HH:MM')}
                            </StyledInfoData>
                        </StyledInfoItem>
                        <StyledInfoItem>
                            <StyledInfoTitle>В пути</StyledInfoTitle>
                            <StyledInfoData>{timeConvert(segment.duration)}</StyledInfoData>
                        </StyledInfoItem>
                        <StyledInfoItem>
                            <StyledInfoTitle>{segment.stops.length} {wordTypoHandler(segment.stops.length)}</StyledInfoTitle>
                            <StyledInfoData>{segment.stops.join(', ')}</StyledInfoData>
                        </StyledInfoItem>
                    </StyledTripInfoWrapper>
                )
            })}
        </TicketCardWrapper>
    )
}

export default TicketCard;