import React from 'react';
import {StyledShowMoreBtn} from "./styled/StyledShowMoreBtn.styled";
import {ITicket} from "../features/TicketsList/TicketsList";
import {SetAllFilters, SetLimitFunc, SetTicketsToShow} from "../utils/types";
import {IFilter} from "../App";

interface IProps {
    setLimit: SetLimitFunc,
    limit: number,
    setTicketsToShow: SetTicketsToShow,
    ticketsToShow: Array<ITicket>,
    allFilters: Array<IFilter>,
    setAllFilters: SetAllFilters,
}

const ShowMoreBtn = (props: IProps) => {

    const handleShowMore = () => {
        const copy = [...props.allFilters]
        const uncheckedAll: Array<IFilter> = copy.map((f: IFilter) => {
            f.checked = false
            return f
        })
        props.setAllFilters(uncheckedAll)
        props.setLimit(props.limit + 5)
    }
    return (
        <StyledShowMoreBtn onClick={handleShowMore}>
            Показать еще 5 билетов!
        </StyledShowMoreBtn>
    )
}

export default ShowMoreBtn;