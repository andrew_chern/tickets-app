import styled from "styled-components";

export const TicketCardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;
  padding: 20px;
  background: var(--white);
  box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.1);
  border-radius: 5px;
  max-width: 502px;
`
export const StyledCardHeader = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
`
export const StyledPrice = styled.p`
  color: var(--blue);
  font-style: normal;
  font-weight: 600;
  font-size: var(--fs-large);
  line-height: 24px;
  margin: 0;
`
export const StyledCarrier = styled.img``

export const StyledTripInfoWrapper = styled.div`
  display: flex;
  margin-bottom: 10px;
  
  &:last-child {
    margin-bottom: 0;
  }
`
export const StyledInfoItem = styled.div`
  display: flex;
  flex-direction: column;
  width: 141px;
  margin-right: 20px;
  
  &:last-child {
    margin-right: 0;
  }
`
export const StyledInfoTitle = styled.span`
  color: var(--grey-light);
  font-style: normal;
  font-weight: 600;
  font-size: var(--fs-main);
  line-height: 18px;
`
export const StyledInfoData = styled.span`
  color: var(--grey-main);
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 21px;
`