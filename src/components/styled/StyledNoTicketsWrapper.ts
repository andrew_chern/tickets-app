import styled from "styled-components";

export const StyledNoTicketsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const StyledOopsWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 20px;

  .oops-icon {
    width: 40px;
    height: 40px;
    margin-right: 10px;
    color: var(--grey-main);
  }
`

export const StyledErrorMessage = styled.span`
  font-family: Open Sans;
  font-style: normal;
  font-weight: 600;
  font-size: var(--fs-medium);
  line-height: 12px;
  color: var(--grey-main);
`

export const StyledReuploadTickets = styled.button`
  background: var(--blue);
  border-radius: 5px;
  font-style: normal;
  font-weight: 600;
  font-size: var(--fs-main);
  line-height: 20px;
  letter-spacing: 0.5px;
  text-transform: uppercase;
  color: var(--white);
  padding: 15px 20px;
  border: none;
  cursor: pointer;
  outline: none;
  
  &:active,
  &:focus-visible {
    outline: none;
  }

  &:hover {
    opacity: 0.8;
  }
`