import styled from "styled-components";

export const StyledShowMoreBtn = styled.button`
  background: var(--blue);
  border-radius: 5px;
  font-style: normal;
  font-weight: 600;
  font-size: var(--fs-main);
  line-height: 20px;
  letter-spacing: 0.5px;
  text-transform: uppercase;
  color: var(--white);
  padding: 15px 20px;
  border: none;
  cursor: pointer;
  
  &:hover {
    opacity: 0.8;
  }
`