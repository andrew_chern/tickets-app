import React from 'react';
import {
    StyledTicketList,
    StyledTicketsHeaderSortItem,
    StyledTicketsListHeader,
    StyledTicketsListWrapper
} from "./StyledTicketsListWrapper.styled";
import TicketCard from "../../components/TicketCard";
import ShowMoreBtn from "../../components/ShowMoreBtn";
import {HandleTicketsSort, SetAllFilters, SetLimitFunc, SetTicketsToShow} from "../../utils/types";
import {IFilter} from "../../App";

export interface ITicket {
    // Цена в рублях
    price: number
    // Код авиакомпании (iata)
    carrier: string,
    // Массив перелётов.
    segments: [
        {
            // Код города (iata)
            origin: string
            // Код города (iata)
            destination: string
            // Дата и время вылета туда
            date: string
            // Массив кодов (iata) городов с пересадками
            stops: string[]
            // Общее время перелёта в минутах
            duration: number
        },
        {
            // Код города (iata)
            origin: string
            // Код города (iata)
            destination: string
            // Дата и время вылета обратно
            date: string
            // Массив кодов (iata) городов с пересадками
            stops: string[]
            // Общее время перелёта в минутах
            duration: number
        }
    ]
}

interface ITicketsListProps {
    ticketsToShow: Array<ITicket>,
    setTicketsToShow: SetTicketsToShow,
    limit: number,
    setLimit: SetLimitFunc,
    currentSortRule: string,
    handleTicketsSort: HandleTicketsSort,
    allFilters: Array<IFilter>,
    setAllFilters: SetAllFilters
}

const TicketsList = (props: ITicketsListProps) => {
    return (
        <StyledTicketsListWrapper>
            <StyledTicketsListHeader>
                <StyledTicketsHeaderSortItem className={props.currentSortRule === 'cheapest' ? 'active' : ''} onClick={() => props.handleTicketsSort('cheapest')}>Самый дешевый</StyledTicketsHeaderSortItem>
                <StyledTicketsHeaderSortItem className={props.currentSortRule === 'fastest' ? 'active' : ''} onClick={() => props.handleTicketsSort('fastest')}>Самый быстрый</StyledTicketsHeaderSortItem>
                <StyledTicketsHeaderSortItem className={props.currentSortRule === 'optimal' ? 'active' : ''} onClick={() => props.handleTicketsSort('optimal')}>Оптимальный</StyledTicketsHeaderSortItem>
            </StyledTicketsListHeader>
            <StyledTicketList>
                {props.ticketsToShow && props.ticketsToShow.map((t, index) => {
                    return <TicketCard ticket={t} key={index}/>
                })}
            </StyledTicketList>
            <ShowMoreBtn
                setLimit={props.setLimit}
                limit={props.limit}
                setTicketsToShow={props.setTicketsToShow}
                ticketsToShow={props.ticketsToShow}
                allFilters={props.allFilters}
                setAllFilters={props.setAllFilters}
            />
        </StyledTicketsListWrapper>
    )
}

export default TicketsList;