import styled from "styled-components";

export const StyledTicketsListWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

export const StyledTicketsListHeader = styled.div`
  display: flex;
  overflow: hidden;
  margin-bottom: 20px;
  box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.1);
  border-radius: 5px;
`

export const StyledTicketsHeaderSortItem = styled.button`
  width: 100%;
  text-align: center;
  padding: 15px;
  border: none;
  border-right: 1px solid #DFE5EC;
  background: var(--white);
  cursor: pointer;
  font-style: normal;
  font-weight: 600;
  font-size: var(--fs-main);
  line-height: 20px;
  letter-spacing: 0.5px;
  text-transform: uppercase;
  color: var(--grey-main);
  box-shadow: none;

  &.active {
    background: var(--blue);
    color: var(--white);
    
    &:hover {
      background: var(--blue);
      color: var(--white);
    }
  }
  
  &:hover {
    background: var(--blue-light);
  }
  
  &:last-child {
    border-right: transparent;
  }
`
export const StyledTicketList = styled.div`
  display: flex;
  flex-direction: column;
`