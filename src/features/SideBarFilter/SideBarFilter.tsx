import React, {SyntheticEvent, useEffect, useState, useRef} from 'react'
import {StyledFilterList, StyledFilterTitle, StyledFilterWrapper} from "./StyledSideBarFilter.styled"
import {useAppSelector} from "../../app/hooks";
import {selectTickets} from "../TicketsList/ticketsSlice";
import {ITicket} from "../TicketsList/TicketsList";
import {FilterFunc, SetAllFilters, SetTicketsToShow} from "../../utils/types";
import {IFilter} from '../../App'
import {filters} from "../../utils/filtersMockData";

interface ISideBarFilterProps {
    ticketsToShow: Array<ITicket>,
    setTicketsToShow: SetTicketsToShow,
    limit: number,
    allFilters: Array<IFilter>,
    setAllFilters: SetAllFilters
}
interface IAppliedFilter {
    funcName: string,
    filterFunc: FilterFunc
}

const SideBarFilter = (props: ISideBarFilterProps) => {
    const [appliedFilters, setAppliedFilters] = useState<Array<IAppliedFilter>>([])
    // @ts-ignore
    const tickets: Array<ITicket> = useAppSelector(selectTickets)

    useEffect(() => {
        if (appliedFilters.length === 0) {
            props.setTicketsToShow(tickets.slice(0, props.limit))
        } else {
            const copy = [...tickets.slice(0, props.limit)]
            const filteredTickets = copy.filter(ticket => {
                const t = appliedFilters.map(filter => {
                    return filter.filterFunc(ticket)
                })
                if (t.some(key => key === true)) {
                    return true
                }
            })
            props.setTicketsToShow(filteredTickets)
        }
    },[appliedFilters])

    const handleFilterCheck = (e: SyntheticEvent): void => {
        const isChecked: boolean = (e.target as HTMLInputElement).checked
        const value: string = (e.target as HTMLInputElement).value
        const copy = [...props.allFilters]
        switch (value) {
            case 'all':
                if (isChecked) {
                    const checkedAll: Array<IFilter> = copy.map((f: IFilter) => {
                        f.checked = true
                        return f
                    })
                    props.setAllFilters(checkedAll)
                    props.setTicketsToShow(tickets.slice(0, props.limit))
                    setAppliedFilters([])
                } else {
                    const checkedAll: Array<IFilter> = copy.map((f: IFilter) => {
                        f.checked = false
                        return f
                    })
                    props.setAllFilters(checkedAll)
                    props.setTicketsToShow(tickets.slice(0, props.limit))
                    setAppliedFilters([])
                }
                break;
            case 'without-stops':
                if (isChecked) {
                    props.setAllFilters(copy.map(f => {
                        if (f.value === value) {
                            f.checked = true
                        }
                        return f
                    }))
                    setAppliedFilters(prevState => [...prevState, {funcName: value, filterFunc: (ticket:ITicket) => ticket.segments[0].stops.length === 0 && ticket.segments[1].stops.length === 0}])
                } else {
                    props.setAllFilters(copy.map(f => {
                        if (f.value === value || f.value === 'all') {
                            f.checked = false
                        }
                        return f
                    }))
                    if (appliedFilters.length > 0) {
                        setAppliedFilters(appliedFilters.filter((f: IAppliedFilter) => f.funcName !== value))
                    } else {
                        const checkedAll: Array<IFilter> = copy.map((f: IFilter) => {
                            f.checked = true
                            return f
                        })
                        props.setAllFilters(checkedAll)
                    }
                }
                break;
            case '1 stop':
                if (isChecked) {
                    props.setAllFilters(copy.map(f => {
                        if (f.value === value) {
                            f.checked = true
                        }
                        return f
                    }))
                    setAppliedFilters(prevState => [...prevState, {funcName: value, filterFunc: (ticket:ITicket) => ticket.segments[0].stops.length + ticket.segments[1].stops.length === 1}])
                } else {
                    props.setAllFilters(copy.map(f => {
                        if (f.value === value || f.value === 'all') {
                            f.checked = false
                        }
                        return f
                    }))
                    setAppliedFilters(appliedFilters.filter((f: IAppliedFilter) => f.funcName !== value))
                }
                break;
            case '2 stops':
                if (isChecked) {
                    props.setAllFilters(copy.map(f => {
                        if (f.value === value) {
                            f.checked = true
                        }
                        return f
                    }))
                    setAppliedFilters(prevState => [...prevState, {funcName: value, filterFunc: (ticket:ITicket) => ticket.segments[0].stops.length + ticket.segments[1].stops.length === 2}])
                } else {
                    props.setAllFilters(copy.map(f => {
                        if (f.value === value || f.value === 'all') {
                            f.checked = false
                        }
                        return f
                    }))
                    setAppliedFilters(appliedFilters.filter((f: IAppliedFilter) => f.funcName !== value))
                }
                break;
            case '3 stops':
                if (isChecked) {
                    props.setAllFilters(copy.map(f => {
                        if (f.value === value) {
                            f.checked = true
                        }
                        return f
                    }))
                    setAppliedFilters(prevState => [...prevState, {funcName: value, filterFunc: (ticket:ITicket) => ticket.segments[0].stops.length + ticket.segments[1].stops.length === 3}])
                } else {
                    props.setAllFilters(copy.map(f => {
                        if (f.value === value || f.value === 'all') {
                            f.checked = false
                        }
                        return f
                    }))
                    setAppliedFilters(appliedFilters.filter((f: IAppliedFilter) => f.funcName !== value))
                }
                break;
            default:
                break;
        }
    }
    useEffect(() => {
        props.setAllFilters(filters)
    }, [])

    return (
        <StyledFilterWrapper>
            <StyledFilterTitle>
                Количество пересадок
            </StyledFilterTitle>
            <StyledFilterList>
                {props.allFilters?.map((f: IFilter, index: number) => {
                    return (
                        <label key={index}>
                            <input
                                type='checkbox'
                                className='checkbox-hidden'
                                value={f.value}
                                name={f.name}
                                checked={f.checked}
                                onChange={(e) => handleFilterCheck(e)} />
                            <span className='custom-checkbox'></span>
                            {f.name}
                        </label>
                    )
                })}
            </StyledFilterList>
        </StyledFilterWrapper>
    )
}

export default SideBarFilter;