import styled from "styled-components";

export const StyledFilterWrapper = styled.div`
  display: flex;
  flex-direction: column;
  background: var(--white);
  margin-right: 20px;
  padding: 17px 0 10px;
  height: 100%;
  box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.1);
  border-radius: 5px;
  min-width: 232px;
`
export const StyledFilterTitle = styled.h2`
  font-weight: 600;
  font-size: var(--fs-main);
  line-height: 12px;
  color: var(--grey-main);
  letter-spacing: 0.5px;
  text-transform: uppercase;
  padding: 0 20px;
  margin: 0 0 15px 0;
`
export const StyledFilterList = styled.form`
  padding: 0;
  margin: 0;
  
  label {
    display: flex;
    padding: 10px 20px;
    cursor: pointer;
    font-style: normal;
    font-weight: normal;
    font-size: var(--fs-medium);
    line-height: 20px;
    align-items: center;
    color: var(--grey-main);
    
    &:hover {
      background: var(--blue-light);
    }

    .checkbox-hidden {
      position: absolute;
      width: 1px;
      height: 1px;
      margin: -1px;
      border: 0;
      clip: rect(0 0 0 0);
      overflow: hidden;
    }

    .custom-checkbox {
      height: 18px;
      width: 18px;
      border: 1px solid #9ABBCE;
      border-radius: 3px;
      margin-right: 10px;
      position: relative;
      
      &:after {
        content: "";
        left: 6px;
        top: 2px;
        width: 5px;
        height: 10px;
        border: solid var(--blue);
        border-width: 0 2px 2px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
        position: absolute;
        display: none;
      }
    }
    
    .checkbox-hidden:checked + .custom-checkbox {
      &:after {
        display: block;
      }
    }
  }
  
`