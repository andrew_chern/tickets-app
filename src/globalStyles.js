import { createGlobalStyle } from 'styled-components';
import OpenSansNormal from './assets/fonts/OpenSans-Regular.ttf';

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'Open Sans', sans-serif;
    src: local('Open Sans'), local('OpenSans'),
    url(${OpenSansNormal}) format('ttf');
    font-weight: 400;
    font-style: normal;
  }
  
  body {
    
    //color's variables
    --white: #FFFFFF;
    --grey-main: #4A4A4A;
    --grey-light: #A0B0B9;
    --blue: #2196F3;
    --blue-light: #F1FCFF;
    --background: #F3F7FA;
    
    
    //font size variables
    --fs-main: 12px;
    --fs-medium: 13px;
    --fs-large: 24px;
    
    margin: 0;
    padding: 0;
    background: var(--background);
    font-family: Open-Sans, Helvetica, Sans-Serif;
    box-sizing: border-box;
  }
`;

export default GlobalStyle;