import styled from "styled-components";

export const StyledAppWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  max-width: 754px;
  min-height: 70vh;
  padding: 50px;
`

export const StyleAppHeader = styled.header`
  text-align: center;
  margin-bottom: 50px;
`

export const StyledAppBody = styled.div`
  display: flex;
  justify-content: center;
  flex: 1 1 auto;
`