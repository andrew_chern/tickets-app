import {ITicket} from "../features/TicketsList/TicketsList";
import {IFilter} from "../App";

export type SetLimitFunc = (newLimit: number) => void
export type SetTicketsToShow = (sortedTickets: Array<ITicket>) => void
export type SetCurrentSortRule = (sortRule: string) => void
export type HandleTicketsSort = (rule: string) => void
export type FilterFunc = (ticket: ITicket) => boolean
export type SetAllFilters = (filter: Array<IFilter>) => void