import {IFilter} from "../App";

export const filters: Array<IFilter> = [
    {
        name: 'Все',
        checked: false,
        value: 'all'
    },
    {
        name: 'Без пересадок',
        checked: false,
        value: 'without-stops'
    },
    {
        name: '1 пересадка',
        checked: false,
        value: '1 stop'
    },
    {
        name: '2 пересадки',
        checked: false,
        value: '2 stops'
    },
    {
        name: '3 пересадки',
        checked: false,
        value: '3 stops'
    },
]