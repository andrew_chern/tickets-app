import React, {useEffect, useState} from 'react';
import {StyleAppHeader, StyledAppBody, StyledAppWrapper,} from './StyledAppWrapper.styled'
import HeaderLogo from './assets/icons/Logo.svg'
import SideBarFilter from "./features/SideBarFilter/SideBarFilter";
import TicketsList, {ITicket} from "./features/TicketsList/TicketsList";
import GlobalStyles from './globalStyles';
import {useAppDispatch, useAppSelector} from "./app/hooks";
import {fetchTicketsAsync, selectError, selectTickets} from "./features/TicketsList/ticketsSlice";
import NoTiketsWrapper from "./components/NoTiketsWrapper";

export interface IFilter {
    name: string,
    checked: boolean,
    value: string
}

function App() {
    const [allFilters, setAllFilters] = useState<Array<IFilter>>([])
    const [ticketsToShow, setTicketsToShow] = useState<Array<ITicket>>([])
    const [limit, setLimit] = useState<number>(5)
    const [currentSortRule, setCurrentSortRule] = useState<string>('')
    // @ts-ignore
    const tickets: Array<ITicket> = useAppSelector(selectTickets)
    const error: string | undefined = useAppSelector(selectError)

    const dispatch = useAppDispatch()

    useEffect(() => {
        dispatch(fetchTicketsAsync())
    }, [])

    useEffect(() => {
        setTicketsToShow(tickets.slice(0, limit))
    }, [tickets, limit])

    useEffect(() => {
        handleTicketsSort(currentSortRule)
    }, [ticketsToShow.length])

    const handleTicketsSort = (rule: string) => {
        const copy = [...ticketsToShow]
        switch (rule) {
            case 'fastest':
                const fastest = copy.sort((a: ITicket, b: ITicket) => {
                    return (a.segments[0].duration + a.segments[1].duration) - (b.segments[0].duration + b.segments[1].duration)
                })
                setTicketsToShow(fastest)
                setCurrentSortRule('fastest')
                break;
            case 'cheapest':
                const cheapest = copy.sort((a: ITicket, b: ITicket) => {
                    return a.price - b.price
                })
                setTicketsToShow(cheapest)
                setCurrentSortRule('cheapest')
                break;
            case 'optimal':
                const optimal = copy.sort((a: ITicket, b: ITicket) => {
                    return (a.segments[0].stops.length + a.segments[1].stops.length) - (b.segments[0].stops.length + b.segments[1].stops.length)
                })
                setTicketsToShow(optimal)
                setCurrentSortRule('optimal')
                break;
            default:
                break;
        }
    }

    return (
        <StyledAppWrapper>
            <GlobalStyles />
            <StyleAppHeader>
                <img src={HeaderLogo} alt='header-logo'/>
            </StyleAppHeader>
            <StyledAppBody>
                {!error ? <>
                    <SideBarFilter
                        ticketsToShow={ticketsToShow}
                        setTicketsToShow={setTicketsToShow}
                        limit={limit}
                        allFilters={allFilters}
                        setAllFilters={setAllFilters}
                    />
                    <TicketsList
                        ticketsToShow={ticketsToShow}
                        setTicketsToShow={setTicketsToShow}
                        limit={limit}
                        setLimit={setLimit}
                        currentSortRule={currentSortRule}
                        handleTicketsSort={handleTicketsSort}
                        allFilters={allFilters}
                        setAllFilters={setAllFilters}
                    />
                </> : <NoTiketsWrapper />
                }
            </StyledAppBody>
        </StyledAppWrapper>
    );
}

export default App;
